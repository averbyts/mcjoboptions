from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 10000

process = """
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > w+ [QCD]
output -f"""

process_dir = new_process(process)

#Fetch default NLO run_card.dat and set parameters
settings = {'nevents':nevents}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""
set aewm1 100
launch
set aewm1 200
launch
set aewm1 300""")
reweight_card_f.close()

generate(process_dir=process_dir,runArgs=runArgs,reweight_card=reweight_card)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

evgenConfig.generators = ["aMcAtNlo"]

############################
# Shower JOs will go here
theApp.finalize()
theApp.exit()
