import MadGraphControl.MadGraph_PDF4LHC15NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 10000

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > t t~ [QCD] @0
    output -f
    """
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION
    
#Fetch default LO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8',
          'nevents':nevents}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Set up MadSpin card
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
fMadSpinCard = open(madspin_card,'w')
fMadSpinCard.write('import '+process_dir+'/Events/'+MADGRAPH_RUN_NAME+'/events.lhe.gz\n')
fMadSpinCard.write('set ms_dir '+process_dir+'/MadSpin\n')
fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
fMadSpinCard.write('''set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event (default: 400)
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
launch''')
fMadSpinCard.close()

try:
    generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
    for an_arg in rte.args:
        if 'Gridpack sucessfully created' in an_arg:
            print 'Handling exception and exiting'
            theApp.finalize()
            theApp.exit()
    print 'Unhandled exception - re-raising'
    raise rte

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################
# Shower JOs will go here
theApp.finalize()
theApp.exit()
