
#--------------------------------------------------------------
# Standard pre-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

#--------------------------------------------------------------
# Some options for local testing.  Shouldn't hurt anything in production.
#
# Nominal configuration for production: MadSpin decays, with 0,1,2+ partom emissions in the matrix element
#
#--------------------------------------------------------------
# Interpret the name of the JO file to figure out the mass spectrum
#
def MassToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

#--------------------------------------------------------------
# split up the JO file input name to interpret it
# e.g. jofile: MadGraphControl_SimplifiedModel_N2N3_ZZ_800_200_MadSpin.py
splitConfig = jofile.rstrip('.py').split('_')

#C1/N2/N3 degenerate
masses['1000025'] = MassToFloat(splitConfig[4])
masses['1000023'] = MassToFloat(splitConfig[4])
masses['1000024'] = MassToFloat(splitConfig[4])
#N1
masses['1000022'] = MassToFloat(splitConfig[5])
if masses['1000022']<0.5: masses['1000022']=0.5

# interpret the generation type, so we know which processes to run.
gentype = splitConfig[2] #will be N2N3

# decaytype is fixed. This really only tells MGC which param card to use.
# In principle this does nothing since it gets overwritten, but we leave
# it in for clarity! The JO needs to have "_HinoZh50_" in the proper position.
decaytype = splitConfig[3] #e.g. ZZ,Zh,hh,HinoZh50

madspindecays=False
if (decaytype == 'ZZ' or decaytype == 'Zh' or decaytype == 'Zh50') and ('MadSpin' in jofile) :
  madspindecays = True;

print "gentype", gentype
print "decaytype", decaytype
print "madspindecays", madspindecays

# max number of jets will be two, unless otherwise specified.
njets = 2

#--------------------------------------------------------------
# MadGraph options
#
process = '''
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~
generate p p > n2 n3 $ susystrong @1
add process p p > n2 n3 j $ susystrong @2
add process p p > n2 n3 j j $ susystrong @3
'''

mergeproc="{n2,1000023}{n3,1000025}"

msdecaystring=""
if madspindecays == True:
  if decaytype == 'ZZ':
    msdecaystring="decay n2 > z n1, z > f f \ndecay n3 > z n1, z > f f\n"
  elif decaytype == 'Zh':
    msdecaystring="decay n2 > z n1, z > f f \ndecay n3 > h01 n1\n"
  elif decaytype == 'Zh50':
    msdecaystring="decay n2 > z n1, z > f f \ndecay n2 > h01 n1\n decay n3 > z n1, z > f f \ndecay n3 > h01 n1\n"


# print the process, just to confirm we got everything right
print "Final process card:"
print process


#--------------------------------------------------------------
# Madspin configuration
#
if madspindecays==True:
  if msdecaystring=="":
    raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
  madspin_card='madspin_card_N2N3_'+decaytype+'.dat'

  mscard = open(madspin_card,'w')

  mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
set BW_cut 15                # default for onshell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set spinmode none
# specify the decay for the final state particles

%s


# running the actual code
launch"""%(runArgs.randomSeed,msdecaystring))
  mscard.close()
  mergeproc+="LEPTONS,NEUTRINOS"

#--------------------------------------------------------------
# Pythia options
#
pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]

# information about this generation
evgenLog.info('Registered generation of ~chi20 ~chi30 production, decaying into ~chi10 via ZZ/Zh/hh; grid point decoded into mass point ' + str(masses['1000025']) + ' ' + str(masses['1000022']))
evgenConfig.contact  = [ "shion.chen@cern.ch" ]
evgenConfig.keywords += ['gaugino', 'chargino', 'neutralino']
evgenConfig.description = '~chi20 ~chi30 production, decaying into ~chi10 via ZZ/Zh/hh in simplified model. Br(N2->ZN1)=Br(N3->ZN1)=Br(N2->hN1)=Br(N3->hN1)=0.5, m_N2N3 = %s GeV, m_N1 = %s GeV'%(masses['1000025'],masses['1000022'])

#--------------------------------------------------------------
# No filter at the moment
evt_multiplier=2
evgenLog.info('inclusive processes will be generated')

#--------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Merging options
#
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = pp>{n2,1000023}{n3,1000025}",
                                 "1000025:spinType = 1",
                                 "1000023:spinType = 1" ]
