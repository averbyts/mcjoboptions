import MadGraphControl.MadGraphUtils as mgu

###-- Set things from the Run Arguments

beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

runName = 'run_01'     
nevents = 1000

###-- Write the process card --###

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l = e+ mu+ e- mu-
define v = ve vm ve~ vm~
generate a a > t t~
output -f""")
fcard.close()

process_dir = mgu.new_process()

#Fetch default LO run_card.dat and set parameters

extras = {'lhe_version'  :'3.0', 
          'cut_decays'   :'F', 
          'pdlabel'      : "'lhapdf'",
          'lhaid'        : 82350,
          'use_syst'     : 'False',
          'sys_scalefact':'1.0 0.5 2.0',
          'sys_pdf'      : 'NNPDF23_lo_as_0130_qed'}

mgu.build_run_card(run_card_old = mgu.get_default_runcard(proc_dir=process_dir),
                   run_card_new = 'run_card.dat',
                   nevts        = nevents,
                   rand_seed    = runArgs.randomSeed,
                   beamEnergy   = beamEnergy,
                   extras       = extras,
)

###-- Now manually change the beam type --###
# lpp: 0=No PDF, 1=p, -1=pbar, 2=photon from p, 3=photon from e
mgu.modify_run_card(run_card        = 'run_card.dat',
                    run_card_backup = 'run_card_backup.dat',
                    settings        = {'lpp1' : '2',  
                                       'lpp2' : '2'}, 
                    delete_backup   = False)

###-- Also need to manually change the top mass and width --###
pcard = open('param_card.dat','w')
pcard.write("""BLOCK MASS #
      5 4.700000e+00 # mb
      6 1.725000e+02 # mt
      15 1.777000e+00 # mta
      23 9.118800e+01 # mz
      25 1.250000e+02 # mh
      1 0.000000e+00 # d
      2 0.000000e+00 # u
      3 0.000000e+00 # s
      4 0.000000e+00 # c
      11 0.000000e+00 # e-
      12 0.000000e+00 # ve
      13 0.000000e+00 # mu-
      14 0.000000e+00 # vm
      16 0.000000e+00 # vt
      21 0.000000e+00 # g
      22 0.000000e+00 # a
      24 8.041900e+01 # w+

BLOCK SMINPUTS #
      1 1.325070e+02 # aewm1
      2 1.166390e-05 # gf
      3 1.180000e-01 # as

BLOCK YUKAWA #
      5 4.700000e+00 # ymb
      6 1.725000e+02 # ymt
      15 1.777000e+00 # ymtau

DECAY 6 1.4803000e+00
DECAY 23 2.441404e+00
DECAY 24 2.047600e+00
DECAY 25 6.382339e-03
DECAY 1 0.000000e+00
DECAY 2 0.000000e+00
DECAY 3 0.000000e+00
DECAY 4 0.000000e+00
DECAY 5 0.000000e+00
DECAY 11 0.000000e+00
DECAY 12 0.000000e+00
DECAY 13 0.000000e+00
DECAY 14 0.000000e+00
DECAY 15 0.000000e+00
DECAY 16 0.000000e+00
DECAY 21 0.000000e+00
DECAY 22 0.000000e+00
""")
pcard.close()


###-- Print the cards so they're in the log file -###    
mgu.print_cards(proc_card  = 'proc_card_mg5.dat',
                run_card   = 'run_card.dat',
                param_card = 'param_card.dat',
)

###-- Generate the LHE events --###
mgu.generate(run_card_loc      = 'run_card.dat',
             param_card_loc    = 'param_card.dat',
             mode              = 2, #this controls if you're paralellising things (mode 0 = you're not, 2 = mutlicxore)
             njobs             = 10, #default is 1
             proc_dir          = process_dir,
             run_name          = runName,
             grid_pack         = False, #default
             gridpack_compile  = False, #default
             cluster_type      = None,  #default
             cluster_queue     = None,  #default
             cluster_nb_retry  = None,  #default
             cluster_temp_path = None,  #default
             extlhapath        = None,  #default
             madspin_card_loc  = None,  #default
             required_accuracy = 0.01,  #default
             gridpack_dir      = None,  #default
             nevents           = None,  #default
             random_seed       = None,  #default
             reweight_card_loc = None,  #default
             bias_module       = None,  #default
)

outputDS = mgu.arrange_output(run_name         = runName,
                              proc_dir         = process_dir,
                              outputDS         = runName+'._00001.events.tar.gz',
                              lhe_version      = 3,
                              saveProcDir      = True,
                              runArgs          = runArgs,
                              madspin_card_loc = None,
)


###-- Run Parton Shower if you want to --###
evgenConfig.generators     += ["MadGraph"]
evgenConfig.description    = 'MadGraph_WbWb'
evgenConfig.contact        = ["jhowarth@cern.ch"]
evgenConfig.keywords      += ['ttbar']
evgenConfig.minevents      = 1
