#! /usr/bin/env python

import optparse, sys, math, subprocess, os
from collections import OrderedDict

parser = optparse.OptionParser(usage=__doc__)
parser.add_option("-i", "--input", default="-", dest="INPUT_FILE", metavar="PATH",   help="path to input log.generate")
parser.add_option("-j", "--joFile", default=None, dest="JOFILE", metavar="PATH",   help="path to jO file")
parser.add_option("-u", "--nocpu", default=False, dest="SKIPCPU", action="store_true", help="Ignore CPU timing information.")
parser.add_option("-m", "--mcver", dest="MC_VER", default="mc", help="Specify MCXX campaign")
parser.add_option("-c", "--nocolour", action="store_true", dest="NO_COLOUR", default=False, help="Turn off colour for copying to file")
parser.add_option("-s", "--standalone", action="store_true", dest="STANDALONE", default=False, help="Run based on cvmfs location of files (stand-alone, no mcjoboptions locally)")

opts, fileargs = parser.parse_args()

MCXX='%s.'%opts.MC_VER
location = '/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions' if opts.STANDALONE else '.'
nEventsRequested=0

def sherpaChecks(logFile):    
    file=open(logFile,"r")
    lines=file.readlines()    
    # check each line
    inside = 0
    numexceeds =0
    retriedBlock=False
    for line in lines:
        if "exceeds maximum by" in line:
            numexceeds +=1
            loginfo("- "+line.strip(),"")
        if "Retried events" in line:
            retriedBlock = True
            continue
        if retriedBlock:
            if "}" in line:
                retriedBlock=False
                continue
            if len(line.split('"')) == 1 or len(line.split('->'))== 1:
                continue
            name = line.split('"')[1]
            percent = line.split('->')[1].split("%")[0].strip()
            if float(percent) > 5.:
                logwarn("- retried events "+name+" = ",percent+" % <-- WARNING: more than 5% of the events retried")
            else:
                loginfo("- retried events "+name+" = ",percent+" %")
    if numexceeds*33>int(nEventsRequested):
        logwarn("","WARNING: be aware of: "+str(numexceeds*100./nEventsRequested)+"% of the event weights exceed the maximum by a factor of ten")

def pythia8Checks(logFile,generatorName):
    file=open(logFile,"r")
    lines=file.readlines()
    usesShowerWeights = False
    usesMatchingOrMerging = False
    usesCorrectPowheg = False
    errors = False
    for line in lines:
        if "Pythia8_ShowerWeights.py" in line:
            usesShowerWeights = True
        if "Pythia8_aMcAtNlo.py" in line or "Pythia8_CKKWL_kTMerge.py" in line or "Pythia8_FxFx.py" in line:
            usesMatchingOrMerging = True
        if "Pythia8_Powheg_Main31.py" in line:
            usesCorrectPowheg = True
    if usesShowerWeights and usesMatchingOrMerging:
        logerr("ERROR:","Pythia 8 shower weights buggy when using a matched/merged calculation. Please remove the Pythia8_ShowerWeights.py include.")
        errors = True
    if "Powheg" in generatorName and not usesCorrectPowheg:
        logerr("ERROR:",generatorName+" used with incorrect include file. Please use Pythia8_Powheg_Main31.py")
        errors = True
    if not errors:
        loggood("INFO: Pythia 8 checks:","Passed")		

def herwig7Checks(logFile,generatorName,metaDataDict):
    errors = False
    allowed_tunes=['H7.1-Default', 'H7.1-SoftTune', 'H7.1-BaryonicReconnection']
    if "7.1" in generatorName:
        if metaDataDict['generatorTune'][0] not in allowed_tunes:
            logerr("ERROR:", "Metadata tune set to {0}, which is not in the list of allowed tunes: {1}".format(metaDataDict['generatorTune'][0], allowed_tunes))
            errors = True
        file=open(logFile,"r")
        lines=file.readlines()
        for line in lines:
            if "Herwig7_EvtGen.py" in line:
                logerr("ERROR:","Herwig 7.1 used with wrong include: Herwig7_EvtGen.py. Please use Herwig71_EvtGen.py instead.")
                errors = True
                break
    if not errors:
        loggood("INFO: Herwig 7 checks:","Passed")

def madgraphChecks(logFile):
    file=open(logFile,"r")
    lines=file.readlines()
    no_base_fragment=False
    for line in lines:
        if "No pdf base fragment" in line:
            no_base_fragment=True
        if "event_norm" in line and 'sum' in line and not 'average' in line:
            logwarn("","The use of event_norm=sum will almost always result in the sample having the wrong total cross section -- please double check that event_norm=average is set in the param_card.dat.")
        if "We need to recalculate the branching fractions" in line:
            br_particles=[p.strip() for p in line.split('for')[-1].split(',')]
            bad_br_particles=[p for p in br_particles if p in ['t','t~','w+','w-','z','h']]
            if len(bad_br_particles)>0:
                logwarn("","MadWidth is used to calculate the branching ratios of {}. This is only LO accurate. For more accurate BRs, please set them explictly in the param_card.dat.".format(",".join(bad_br_particles)))
    if no_base_fragment:
        logwarn("","No PDF base fragment was included, which is the recommended way to steer pdf and systematics (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)")

def generatorChecks(logFile, generatorName,metaDataDict):
    print("")
    print("-------------------------")
    print("Generator specific tests: {0}".format(generatorName))
    print("-------------------------")
    if "Sherpa" in generatorName:
        sherpaChecks(logFile)
    if "Pythia8" in generatorName:
        pythia8Checks(logFile,generatorName)
    if "Herwig7" in generatorName:
        herwig7Checks(logFile,generatorName,metaDataDict)
    if "MadGraph" in generatorName:
        madgraphChecks(logFile)

def getNEventsPerJob(jOFile):
    try:
        with open(jOFile) as jO:
            for line in jO:
                # Strip anything after a comment character
                line.split("#")[0]
                if "nEventsPerJob" in line:
                    return line.split('=')[1].strip()
    except:
        raise RuntimeError(f"Problem while reading {jOFile}. If the file doesn't exist consider running without -j /path/to/joFile")
    return "10000"


def main():
    """logParser.py script for parsing log.generate files to check MC production settings and output
     - Written by Josh McFayden <mcfayden@cern.ch> Nov 2016 """

    if opts.INPUT_FILE=="-":
        parser.print_help()
        return 
    
    # define dictionaries with keys as variables to be searched for and values to store the results    
    JOsDict={
        'using release':[],
        #"including file \""+MCJobOptions+"/":[],
        "including file \""+MCXX:[]
        }
    
    testHepMCDict={
        'Events passed':[],
        'Efficiency':[]
        }
    
    countHepMCDict={
        'Events passing all checks and written':[]
        }
    
    evgenFilterSeqDict={
        'Weighted Filter Efficiency':[],
        'Filter Efficiency':[]
        }
    
    simTimeEstimateDict={
        'RUN INFORMATION':[]
        }
    
    metaDataDict={ 
        'physicsComment =':[],
        'generatorName =':[],
        'generatorTune':[],
        'keywords =':[],
        'specialConfig =':[],
        'contactPhysicist =':[],
#        'randomSeed':[],
        'genFilterNames = ':[],
        'cross-section (nb)':[],
        'generator =':[],
        'weights =':[],
        'PDF =':[],
        'GenFiltEff =':[],
        'sumOfNegWeights =':[],
        'sumOfPosWeights =':[]
        }
    
    generateDict={
        'nEventsPerJob':[],
        'Requested output events':[],
        'transform':[],
        'inputFilesPerJob':[],
        }
    
    perfMonDict={
        'snapshot_post_fin':[],
        'jobcfg_walltime':[],
        'last -evt vmem':[]
        }
    
    # open and read log file
    file=open(opts.INPUT_FILE,"r")
    lines=file.readlines()
    
    # check each line
    for line in lines:
    
        checkLine(line,'Py:Athena',JOsDict,'INFO')
    
        checkLine(line,'MetaData',metaDataDict,'=')
        checkLine(line,'Py:Gen_tf',generateDict,'=')
    
        checkLine(line,'Py:PerfMonSvc',perfMonDict,':')
        checkLine(line,'PMonSD',perfMonDict,'---')
    
        checkLine(line,'TestHepMC',testHepMCDict,'=')
        checkLine(line,'Py:EvgenFilterSeq',evgenFilterSeqDict,'=')
        checkLine(line,'CountHepMC',countHepMCDict,'=')
        checkLine(line,'SimTimeEstimate',simTimeEstimateDict,'|')
                
    # print results
    JOsErrors=[]
    print("")
    print("---------------------")
    print("jobOptions and release:")
    print("---------------------")

    #Checking jobOptions
    JOsList=getJOsList(JOsDict)
    if not len(JOsList):
        JOsErrors.append("including file \""+MCXX)
    else:
        if not len(JOsDict["including file \""+MCXX]):
            JOsErrors.append("including file \""+MCXX)
     
    #Checking release
    release="not found"
    if not len(JOsDict['using release']):
        JOsErrors.append(JOsDict['using release'])
    else:
        name='using release'
        tmp=JOsDict[name][0].replace('using release','').strip().split()[0]
        val=tmp.replace('[','').replace(']','')
        flavour=val.split('-')[0]
        release=val.split('-')[1]
        blacklisted=checkBlackList(flavour,release,".",JOsList,location=location)
        # check that release is AthGeneration
        if flavour != "AthGeneration":
            logerr( '- '+name+' = ',"".join(val)+f" <-- ERROR: AthGeneration should be used instead of {flavour}")
        else:
            # check blacklist
            if blacklisted:
                logerr( '- '+name+' = ',"".join(val)+" <-- ERROR: %s"%blacklisted)
            else:
                loggood( '- '+name+' = ',"".join(val))
     
    
    if len(JOsErrors):
        print("---------------------")
        print("MISSING JOs:")
        for i in JOsErrors:
            if i == "including file \""+MCXX:
                logerr("",f"ERROR: jO not found! (log.generate should contain lines like: including file \"{MCXX}*.py\")")
            else:
                logwarn("","WARNING: %s is missing!"%i)
    
    
    ###
    generateErrors=[]
    print("")
    print("---------------------")
    print("Generate params:")
    print("---------------------")
    for key in list(generateDict.keys()):
        name=key
        val=generateDict[key]
        if not len(val):
            generateErrors.append(name)
        else:
            if key == 'nEventsPerJob':
                # Allow to overwrite nEventsPerJob from jO file if specified (or present)
                if opts.JOFILE:
                    nEventsPerJob=getNEventsPerJob(opts.JOFILE)
                else:
                    # No jO file specified, still try to overwrite from JO in same dir
                    jOFile=os.path.join(location,os.path.dirname(opts.INPUT_FILE),JOsList[0])
                    if os.path.exists(jOFile):
                        nEventsPerJob=getNEventsPerJob(jOFile)
                    else:
                        nEventsPerJob=str(val[0]).split('#')[0].strip()
                val=nEventsPerJob
                del generateDict['nEventsPerJob']
            elif key == 'transform':
                releaseNumber=int(release.split(".")[0])*10000+int(release.split(".")[1])*100+int(release.split(".")[2])
                if (val[0] != 'Gen_tf' and val[0] != 'Gen_tf_txt') and releaseNumber > 210610:
                    logerr("","ERROR: tranform = %s and release is %s. Please use Gen_tf or Gen_tf_txt!" %(val[0],release))
            elif key == 'inputFilesPerJob':
                nfiles=val[0].split("#")[0][0]
                val=nfiles
                if int(val) > 100:
                    logerr("", "ERROR: Input files used: %s. Need to use less than 100." % val)
            loginfo( '- '+name+' = ',"".join(val))
    
    if len(generateErrors):
        print("---------------------")
        print("MISSING Generate params:")
        for i in generateErrors:
            logerr("","ERROR: %s is missing!"%i)
            
    # Number of requested output events
    nEventsRequested=int(generateDict["Requested output events"][0])
    
    ###
    metaDataErrors=[]
    print("")
    print("---------------------")
    print("Metadata:")
    print("---------------------")
    for key in metaDataDict:
        name=key.replace("=","").strip()
        val=metaDataDict[key]
        if not len(val):
            metaDataErrors.append(name)
        else:
            if name=="contactPhysicist":
                if '@' in "".join(val):
                    loggood( '- '+name+' = ',"".join(val))
                else:
                    logerr( '- '+name+' = ',"".join(val)+"  <-- ERROR: No email found")
                continue
            elif name=="cross-section (nb)":
                if float(val[0]) < 0:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: Cross-section is negative")
                    continue
            elif name=="keywords":
                kfile = open(location+"/common/evgenkeywords.txt")
                klines = kfile.readlines()
                foundkeywordlist=""
                for keyword in (",".join(val)).split(','):
                    keywordfound=False
                    for line in klines:
                        if line.strip().lower()==keyword.strip().lower():
                            keywordfound=True
                            break
                    if not keywordfound:
                        logwarn( '- '+name+' = ',keyword.strip()+"  <-- WARNING: keyword not found in common/evgenkeywords.txt")
                    else:
                        if len(foundkeywordlist): foundkeywordlist+=","+keyword
                        else: foundkeywordlist=keyword
                if len(foundkeywordlist): loggood( '- '+name+' = ',foundkeywordlist)
                        
                continue
                

            loginfo( '- '+name+' = ',"".join(val))
    
    if len(metaDataDict["sumOfPosWeights ="]) and len(metaDataDict["sumOfNegWeights ="]):
        ratio = float(metaDataDict["sumOfNegWeights ="][0])*1.0/(float(metaDataDict["sumOfPosWeights ="][0]) + float(metaDataDict["sumOfNegWeights ="][0]))
        if ratio>0.15:
            logwarn( '- sumOfNegWeights/(sumOfPosWeights+sumOfNegWeights) = ',str(ratio)+"  <-- WARNING: more than 15% of the weights are negative")


    if len(metaDataErrors):
        print("---------------------")
        print("MISSING Metadata:")
        for i in metaDataErrors:
            if i=="weights" or i=="genFilterNames" or i=="generator" or i=="PDF" or i=="sumOfNegWeights" or i=="sumOfPosWeights":
                loginfo("INFO:","%s is missing"%i)
            else:
                logerr("","ERROR: %s is missing!"%i)
            

    # Generator specific tests
    # First find generator first
    generatorName=metaDataDict['generatorName ='][0]
    generatorChecks(opts.INPUT_FILE, generatorName,metaDataDict)
    
    ####  Event tests
    testDict = {
        'TestHepMC':testHepMCDict,
        'EvgenFilterSeq':evgenFilterSeqDict,
        'CountHepMC':countHepMCDict,
        'SimTimeEstimate':simTimeEstimateDict
        }
    
    testErrors=[]
    filt_eff=1.0
    CountHepMC=0
    print("")
    print("---------------------")
    print("Event tests:")
    print("---------------------")
    for dictkey in testDict:
        for key in testDict[dictkey]:
            name=key
            val=testDict[dictkey][key]
            if not len(val):
                testErrors.append("%s %s"%(dictkey,name))
            else:
                #Check final Nevents processed
                if dictkey=="CountHepMC":
                    nEventsAllowedInProduction = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000]
                    CountHepMC=int(val[0])
                    if not CountHepMC in nEventsAllowedInProduction:
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val)+"  <-- ERROR: Not an acceptable number of events for production (1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000)")
                    elif CountHepMC != nEventsRequested:
                        logerr( '- '+dictkey+" "+name+' = ', f"{val}  <-- ERROR: This is not equal to Requested output events={nEventsRequested}")
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
        
                #Check filter efficiencies are not too low
                if dictkey=="EvgenFilterSeq":
                    if name=="Weighted Filter Efficiency":
                        filt_eff=float(val[0].split()[0])
                    if float(val[0].split()[0])<1e-6:
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val))
                    elif float(val[0].split()[0])<1e-4:
                        logwarn( '- '+dictkey+" "+name+' = ',"".join(val))
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
    
                if dictkey=="TestHepMC" and name=="Efficiency":
                    if float(val[0].replace('%',''))<100. and float(val[0].replace('%',''))>=98.:
                        logwarn( '- '+dictkey+" "+name+' = ',"".join(val))
                    elif float(val[0].replace('%',''))<100.:
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val))
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
    
    
                loginfo( '- '+dictkey+" "+name+' = ',"".join(val))
    
    if len(testErrors):
        print("---------------------")
        print("Failed tests:")
        for i in testErrors:
            if i =="SimTimeEstimate RUN INFORMATION":
                logwarn("","WARNING: %s is missing!"%i)
            else:
                if "TestHepMC" in i and "Sherpa" in metaDataDict['generatorName ='][0]:
                    logwarn("","WARNING: %s is missing, but expected as it's Sherpa!"%i)
                else:
                    logerr("","ERROR: %s is missing!"%i)


    #### Performance tests
    cpuPerJob=0.0
    perfMonErrors=[]
    print("")
    print("---------------------")
    print("Performance metrics:")
    print("---------------------")
    for key in perfMonDict:
        name=key
        val=perfMonDict[key]
        if not len(val):
            perfMonErrors.append(name)
        else:
            if key == 'snapshot_post_fin' and not opts.SKIPCPU:
                name = 'CPU'
                tmp = 0.
                tmp=float(val[0].split()[3])
                if len(perfMonDict['jobcfg_walltime']):
                    tmp+=float(perfMonDict['jobcfg_walltime'][0].split()[1].split('=')[1])
                cpuPerJob=tmp/(1000.*60.*60.)
                # Calculate timing and extrapolate if test run was run with less events
                if CountHepMC != int(nEventsPerJob):
                    print(f"- actual CPU ({CountHepMC} events) = {cpuPerJob:.2f} hrs")
                    cpuPerJob=float(nEventsPerJob)*cpuPerJob/float(CountHepMC)
                    loginfo(f"- CPU extrapolated to {nEventsPerJob} events =", f"{cpuPerJob:.1f} hrs")
                if cpuPerJob > 18.:
                    logerr( f"- {name} = ",f"{cpuPerJob:.2f} hrs  <-- ERROR: Too high CPU time - should be between 6-12h. Adjust nEventsPerJob!")
                elif cpuPerJob >= 6. and cpuPerJob <= 12.:
                    loggood( f"- {name} = ",f"{cpuPerJob:.2f} hrs")
                elif cpuPerJob < 1.:
                    if CountHepMC < 10000:
                        logerr( f"- {name} = ",f"{cpuPerJob:.2f} hrs <-- ERROR: Too low CPU time - should be between 6-12h. Adjust nEventsPerJob!")
                    else:
                        loggood( f"- {name} = ",f"{cpuPerJob:.2f} hrs")
                else:
                    if CountHepMC < 10000:
                        logwarn( f"- {name} = ",f"{cpuPerJob:.2f} hrs  <-- WARNING: CPU time not optimal - should be between 6-12h. Adjust nEventsPerJob!")
                    else:
                        loggood( f"- {name} = ",f"{cpuPerJob:.2f} hrs")
                # Also print timing information for CI - CI runs max(1,0.01*nEventsPerJob)
                CICPU=max(1,0.01*float(nEventsPerJob))*cpuPerJob/float(nEventsPerJob)
                print(f"- estimated CPU for CI job = {CICPU:.2f} hrs")
                
            if key == 'last -evt vmem':
                name = 'Virtual memory'
                tmp=float(val[0].split()[0])
                if tmp > 4000 and tmp < 8000:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: High memory usage - alert MC production team")
                elif tmp > 8000:
                    logerr( '- '+name+' = ',"".join(val)+"  <-- ERROR: Too high memory usage")
                else:
                    loggood( '- '+name+' = ',"".join(val))
    
    
    if len(perfMonErrors):
        print("---------------------")
        print("MISSING Performance metric:")
        for i in perfMonErrors:
            logerr("","ERROR: {0} is missing!".format(i))
            
    
    
    ## Add equivalent lumi information
    print("")
    print("---------------------")
    print(" Others:")
    print("---------------------")

    xs_nb=0.0
    eff_lumi_fb=0.0

    xs_nb=float(metaDataDict['cross-section (nb)'][0])
    eff_lumi_fb=float(nEventsRequested)/(1.E+06*xs_nb*filt_eff)
    if eff_lumi_fb > 1000.:
        logwarn("- Effective lumi (fb-1):", f"{eff_lumi_fb} <-- WARNING: very high effective luminosity")
    elif eff_lumi_fb < 40.:
        logwarn("- Effective lumi (fb-1):", f"{eff_lumi_fb} <-- WARNING: low effective luminosity")
    else:
        loggood("- Effective lumi (fb-1):",str(eff_lumi_fb))
    if nEventsRequested <= 5000:
        logwarn("- Total no. of events:", f"{nEventsRequested} <-- WARNING: This total is low enough that the mu profile may be problematic - INFORM MC PROD")

    # Print total number of Errors/Warnings
    print("")
    print("---------------------")
    print(" Summary:")
    print("---------------------")
    if (LogCounts.Errors == 0):
        if (LogCounts.Warnings == 0):
            loggood("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> OK for production")
        else:
	        loggood("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> Some warnings encountered, check that these are ok before submitting production!")
    else:
        logerr("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> Errors encountered! Not ready for production!")  
    print("")
        
    return 
    


def getJOsList(JOsDict):
    liststr=''
    if len(JOsDict["including file \""+MCXX]):
        if len(liststr): liststr+="|"
        liststr+="|".join(JOsDict["including file \""+MCXX]).replace("nonStandard/","")
    liststr=liststr.replace('/','').replace('"','').replace('including file','').replace(' ','')
    tmplist=liststr.split('|')
    return tmplist


def checkBlackList(branch,cache,outnamedir,JOsList,location) :

    aJOs=[]
    for l in JOsList:
        if MCXX in l:
            aJOs.append(l)   

    isError = None
    tmpblackfile=location+'/common/BlackList_caches.txt'
    bfile = open(tmpblackfile)
    blines=bfile.readlines()
    for line in blines:
        if not line.strip():
            continue

        bad = "".join(line.split()).split(",")
        
        badgens=[bad[2]]
        if bad[2]=="Pythia8":
            badgens.append("Py8")
        if bad[2]=="Pythia":
            badgens.append("Py")
        if bad[2]=="MadGraph":
            badgens.append("MG")
        if bad[2]=="Powheg":
            badgens.append("Ph")
        if bad[2]=="Herwigpp":
            badgens.append("Hpp")
        if bad[2]=="Herwig7":
            badgens.append("H7")
        if bad[2]=="Sherpa":
            badgens.append("Sh")
        if bad[2]=="Alpgen":
            badgens.append("Ag")
        if bad[2]=="EvtGen":
            badgens.append("EG")
        if bad[2]=="ParticleGun":
            badgens.append("PG")
        
        #Match Generator and release type e.g. AtlasProduction, MCProd
        if any( badgen in s.split('_')[0] for s in aJOs for badgen in badgens ) and branch in bad[0]:
            #Match cache
            cacheMatch=True               
            for i,c in enumerate(cache.split('.')):
                if not c == bad[1].split('.')[i]:
                    cacheMatch=False
                    break

            if cacheMatch:            
                #logerr("", "Combination %s_%s for %s it is blacklisted"%(bad[0],bad[1],bad[2]))
                isError = "%s_%s is blacklisted for %s"%(bad[0],bad[1],bad[2])
                return isError

            
    return isError


# find identifiers and variables in log file lines
def checkLine(line, lineIdentifier, dict, splitby ):
    if lineIdentifier in line:
        for param in dict:
            if param=="including file \""+MCXX:
                if "including file" in line and MCXX in line:
                    if len(line.split(splitby))==0:
                        raise RuntimeError("Found bad entry %s"%line)
                    else:
                        thing="".join(line.split(lineIdentifier)[1].split(splitby)[1:]).split("/")[-1].strip()
                        dict[param].append(thing)
                    break
            elif param=="Requested output events":
                if "Requested output events" in line:
                   if len(line.split(splitby))==0:
                       raise RuntimeError("Found bad entry %s"%line)
                   else:
                       thing="".join(line.split(lineIdentifier)[1].split(" ")[-1]).strip()
                       dict[param].append(thing)
                   break
            else:
                if param in line:
                    if len(line.split(splitby))==0:
                        raise RuntimeError("Found bad entry %s"%line)
                    else:
                        thing="".join(line.split(lineIdentifier)[1].split(splitby)[1:]).strip()     
                        dict[param].append(thing)
                    break
		    
class bcolors:
    if not opts.NO_COLOUR:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
    else:
        HEADER = ''
        OKBLUE = ''
        OKGREEN = ''
        WARNING = ''
        FAIL = ''
        ENDC = ''

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

class LogCounts:
    Errors = 0
    Warnings = 0

def loginfo(out1,out2):
    print("{0:s} {1:s} {2:s} {3:s}".format(str(out1), bcolors.OKBLUE, str(out2), bcolors.ENDC))
def loggood(out1,out2):
    print("{0:s} {1:s} {2:s} {3:s}".format(str(out1), bcolors.OKGREEN, str(out2), bcolors.ENDC))
def logerr(out1,out2):
    print("{0:s} {1:s} {2:s} {3:s}".format(str(out1), bcolors.FAIL, str(out2), bcolors.ENDC))
    LogCounts.Errors += 1
def logwarn(out1,out2):
    print("{0:s} {1:s} {2:s} {3:s}".format(str(out1), bcolors.WARNING, str(out2), bcolors.ENDC))
    LogCounts.Warnings += 1

def pad(seq, target_length, padding=None):
    length = len(seq)
    if length > target_length:
        return seq
    seq.extend([padding] * (target_length - length))
    return seq

if __name__ == "__main__":
    main()
    
    
