evgenConfig.generators  += ["Powheg"]
evgenConfig.description  = 'Powheg WWbb production, including interference between ttbar and Wt.'
evgenConfig.keywords    += [ 'SM', 'top', 'WW', 'lepton']
evgenConfig.contact      = [ 'Marcel Niemeyer <marcel.niemeyer@cern.ch>', 'Simone Amoroso <simone.amoroso@cern.ch>' ]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg bblvlv process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bblvlv_Common.py")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.nEvents=2000
PowhegConfig.ncall1=5000000
PowhegConfig.width_t=-1
PowhegConfig.for_reweighting=1

PowhegConfig.PDF=10800

PowhegConfig.generate()
