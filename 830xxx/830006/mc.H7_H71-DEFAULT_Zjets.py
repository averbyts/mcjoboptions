# based on the JobOptions MC15.429313 and MC15.429721

evgenConfig.generators  = ["Herwig7"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.description = "Herwig7 BuiltinME Z + jets with MMHT2014 LO PDF and H7.1-Default tune"
evgenConfig.keywords    = ["SM", "Z", "jets"]
evgenConfig.contact     = ['paolo.francavilla@cern.ch', 'Shu.Li@cern.ch', "daniel.rauch@desy.de", "andrii.verbytskyi@mpp.mpg.de"]
evgenConfig.nEventsPerJob    = 5000

# initialize Herwig7 generator configuration for built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")


#this is the content of Herwig7_TheP8I.py-->
assert hasattr(genSeq, "Herwig7")

#The path is hardcoded, exactly in a way that is done for OpenLoops
try:
# This should be a prefix      
      TheP8I_path=os.environ['THEP8I_PATH']
except:
      TheP8I_path='/cvmfs/sft.cern.ch/lcg/releases/LCG_88/MCGenerators/thep8i/2.0.0/x86_64-slc6-gcc62-opt'
os.environ["LD_LIBRARY_PATH"]=TheP8I_path+"/lib64/ThePEG"+":"+TheP8I_path+"/lib/ThePEG"+":"+os.environ["LD_LIBRARY_PATH"]

try:
      TheP8I_PYTHIA8DATA=os.environ['PYTHIA8DATA']
except:
      TheP8I_PYTHIA8DATA='/cvmfs/sft.cern.ch/lcg/releases/LCG_88/MCGenerators/pythia8/244/x86_64-slc6-gcc62-opt/share/Pythia8/xmldoc/'
os.environ['PYTHIA8DATA']=TheP8I_PYTHIA8DATA


Herwig7Config.add_commands("""
mkdir /TheP8I
create TheP8I::StringFragmentation  /TheP8I/Handlers/StringFrag/StringFragmenter    libTheP8I.so
create TheP8I::TheP8IStrategy  /TheP8I/StdStrategy  libTheP8I.so
cd /Herwig/Generators
set EventGenerator:EventHandler:HadronizationHandler /TheP8I/Handlers/StringFrag/StringFragmenter
set EventGenerator:Strategy /TheP8I/StdStrategy
""")


#########################################<--

# configure Herwig7
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")
Herwig7Config.tune_commands()


Herwig7Config.add_commands("""
## Z+jet
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEZJet
""")

# run Herwig7
Herwig7Config.run()
